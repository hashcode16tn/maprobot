package ressource;

import java.util.ArrayList;

public class File {
	ArrayList<String> lines=new ArrayList<String>();

	public void addLine(String l) {
		lines.add(l);
	}

	public String getLine(int i) {
		return lines.get(i);
	}

	public int nbrOfLines() {
		return lines.size();
	}

	public String getText() {
		StringBuilder sb = new StringBuilder() ;

		for(String s : lines){
			sb.append(s).append(System.lineSeparator()) ;
		}

		return sb.toString() ;
	}
}
