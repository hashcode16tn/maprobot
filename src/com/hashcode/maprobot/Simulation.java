package com.hashcode.maprobot;

import java.util.ArrayList;

import ressource.Node;
import ressource.Tree;

import com.sun.javafx.geom.Vec2f;

public class Simulation {

	ArrayList<String> types;
	int longueur;
	int largeur;
	int dechets = 0;
	int[] pointer1 = new int[2];
	int[] pointer2 = new int[2];
	Node n;


	Map map ;
	ArrayList<Robot> robots ;
	ArrayList<Vec2f> starts ;

	ArrayList<Node> Trees;

	final static Integer MAX_DEPTH = 10 ;

	public Simulation(Map map, ArrayList<Robot> robots) {
		this.map = map ;
		this.robots = robots ;
		starts = map.getStartpoint() ;
		run() ;
	}

	public void run() {
		for(Robot robot : robots){
			for(Vec2f p : starts){
				simulateFrom(p, robot) ;
			}
		}
	}


	private int simulateFrom(Vec2f p, Robot robot) {
		// TODO Auto-generated method stub
		Tree t = createTree(p) ;

		ArrayList<Integer> perf = calculatePerf(t.root(), robot, map, new ArrayList<Integer>());
		//perf.sort();
		return perf.get(0);
	}

	private ArrayList<Integer> calculatePerf(Node n, Robot robot, Map map2, ArrayList<Integer> perfs) {
		// TODO Auto-generated method stub
		Node root = n ;
		Node n1 = n;
		int p = 0 ;
		while(n.getChildren().size() > 0){
			n1 = n ;
			n = n.getChildren().get(0);
			p = p + 1;
			// Calc perfo here
		}
		// remove the child
		n1.getChildren().remove(0) ;
		perfs.add(p);

		return calculatePerf(root, robot, map2, perfs) ;
	}

	private Tree createTree(Vec2f p) {
		// TODO Auto-generated method stub
		Tree t = new Tree((int) p.x,(int) p.y);
		ArrayList<Vec2f> visitedPoints = new ArrayList<Vec2f>() ;
		visitedPoints.add(p) ;

		Node root = t.root() ;

		addFourChildren((int)p.x, (int)p.y, root, visitedPoints) ;

		createTreeRecu(root, visitedPoints) ;

		return t ;
	}

	private void createTreeRecu(Node n, ArrayList<Vec2f> visitedPoints) {
		// TODO Auto-generated method stub
		for(Node child : n.getChildren()){
			ArrayList<Vec2f> visitedPointsR = new ArrayList<Vec2f>() ;
			visitedPointsR.addAll(visitedPoints) ;
			addFourChildren(child.getX(), child.getY(), child, visitedPoints);
			if(child.getDepth() < MAX_DEPTH) // stop condition
				createTreeRecu(child, visitedPointsR) ;
		}
	}

	private boolean correctIndex(int x, int y, Node p, ArrayList<Vec2f> visitedPoints) {
		if (x >= 0 && y >= 0 && x < largeur && y < longueur && x != p.getX() && y != p.getY() && !visitedPoints.contains(new Vec2f(x,y))){
			if(!map.isBlocked(x, y)){
				return true;
			}
		}
		return false;
	}

	private void addFourChildren(int x, int y, Node p, ArrayList<Vec2f> visitedPoints) {
		if (correctIndex(x + 1, y, p, visitedPoints)){
			p.addChild(x, y);
		}
		if (correctIndex(x, y + 1, p, visitedPoints)){
			p.addChild(x, y);
		}
		if (correctIndex(x - 1, y, p, visitedPoints)){
			p.addChild(x, y);
		}
		if (correctIndex(x, y - 1, p, visitedPoints)){
			n.addChild(x, y);
		}
	}

}
