package com.hashcode.maprobot;

import java.util.ArrayList;

import ressource.File;

public class Robot {
	private File myFile ;
	private int number ;
	private String name ;
	private String capBattery ;
	private String VperB ;
	private String VperD ;
	
	public Robot(String str){
		ArrayList<String> listOfStr = new ArrayList<String>() ;
		String line = "";
		for(int i=0;i<str.length();i++){
			
			if(str.charAt(i)!='\n'){
				line = str.substring(i,i+1) ;
			}
		     if(str.charAt(i)=='\n'){
		    	 line=str.substring(line.indexOf(':')+1,line.length());
		    	  listOfStr.add(line) ;
		    	  line ="" ;
			}
		  
		    
		}
			name=listOfStr.get(0);
			capBattery=listOfStr.get(1);
			VperB=listOfStr.get(2) ;
			VperD=listOfStr.get(3) ;
			
		
			
		
		
		
			
		
		
	}

	public String getName() {
		return name ;
	}

	public String getCapBattery() {
		return capBattery;
	}

	public String getVperB() {
		return VperB;
	}

	public String getVperD() {
		return VperD;
	}

	
	
	public static ArrayList<Robot> getListOfRobots(File myFile){
		ArrayList<Robot> listOfRobots = new ArrayList<>() ;
		StringBuilder sb = new StringBuilder() ;
		for(int i=0 ;i<myFile.nbrOfLines();i++){
			if(myFile.getLine(i).charAt(0)!='#'){
				sb.append(myFile.getLine(i)).append(System.lineSeparator());		
			}
			if(myFile.getLine(i).charAt(0)=='#'){
				Robot r = new Robot(sb.toString()) ;
				sb = new StringBuilder() ;
				listOfRobots.add(r) ;		
			}
			
		}
		return listOfRobots;
	}
	
	

}
